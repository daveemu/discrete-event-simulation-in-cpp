#include "CPSimulation.h"

CPSimulation::CPSimulation(std::size_t time, std::size_t items, std::size_t buffersize)
	: max_time{time}, max_items{items}, buffer(buffersize)
{
		for(size_t i = 0; i < (2*max_items); i++) {
			size_t newTime = rand() % 100;
			if((rand() % 10) < 5) {
				event_queue.push(new ConsumerEvent(newTime, event_queue, buffer));
			} else {
				event_queue.push(new ProducerEvent(newTime, event_queue, buffer));
			}
		}
}

void CPSimulation::run() {

	while((!event_queue.empty()) && (current_time < max_time) && (current_items < max_items) ) {
		auto tmp = event_queue.top();
		event_queue.pop();
		current_time += tmp->get_time();
		if(tmp->execute()) {
			++current_items;
		}
		delete tmp;
		buffer.print();
	}
}

