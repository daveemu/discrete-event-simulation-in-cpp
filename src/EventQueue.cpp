#include "EventQueue.h"

EventQueue::~EventQueue() {
	while(!event_queue.empty()) {
		delete event_queue.top();
		event_queue.pop();
	}
}

void EventQueue::add(Event* e) {
	event_queue.push(e);
}

void EventQueue::run() {

	while((!event_queue.empty()) && (!event_queue.top()->is_stop_event())) {
		event_queue.top()->execute();
		delete event_queue.top();
		event_queue.pop();
	}

}

void EventQueue::step() {
	event_queue.top()->execute();
	event_queue.pop();
}
