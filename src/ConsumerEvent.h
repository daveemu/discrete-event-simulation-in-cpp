#ifndef CONSUMEREVENT_H_
#define CONSUMEREVENT_H_

#include "Event.h"
#include "Buffer.h"
#include <queue>

class ConsumerEvent : public Event {
private:
	std::priority_queue<prio_type>& queue;
	Buffer<int>& buffer;

public:
	ConsumerEvent(size_t t, std::priority_queue<prio_type>& q, Buffer<int> &b)
	: Event(t, "ConsumerEvent", false), queue{q}, buffer{b} {} ;
	virtual ~ConsumerEvent() = default;
	virtual bool execute() {
		if(!buffer.is_empty()) {
			buffer.take();
			return true;
		} else {
			size_t newTime = rand() % 10;
			queue.push(new ConsumerEvent(newTime, queue, buffer));
			return false;
		}
	};
};

#endif /* CONSUMEREVENT_H_ */
