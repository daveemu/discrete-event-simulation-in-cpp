#ifndef EVENTQUEUE_H_
#define EVENTQUEUE_H_

#include "Event.h"
#include <queue>

#define prio_type Event*, std::vector<Event*>, greater_ptr<Event*>

template <typename T>
struct greater_ptr {
  bool operator()(const T& lhs, const T& rhs) const {
    return *lhs > *rhs;
  }
};

class EventQueue {
protected:
	std::priority_queue<prio_type> event_queue;

public:
	EventQueue() = default;
	virtual ~EventQueue();
	virtual void add(Event *e);
	virtual void run();
	virtual void step();
	virtual size_t size() { return event_queue.size(); };

};

#endif /* EVENTQUEUE_H_ */
