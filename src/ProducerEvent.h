#ifndef PRODUCEREVENT_H_
#define PRODUCEREVENT_H_

#include "EventQueue.h"
#include "Event.h"
#include "Buffer.h"
#include <queue>

#define PRODUCT 13 //random number, simulates a product

class ProducerEvent : public Event{
private:
	std::priority_queue<prio_type>& queue;
	Buffer<int>& buffer;

public:
	ProducerEvent(std::size_t t, std::priority_queue<prio_type>& q, Buffer<int> &b)
	: Event(t, "ProducerEvent", false), queue{q}, buffer{b} {};

	virtual ~ProducerEvent() = default;

	virtual bool execute() {
		if(!buffer.is_full()) {
			buffer.add(PRODUCT);
			return true;
		} else {
			size_t newTime = rand() % 10;
			queue.push(new ProducerEvent(newTime, queue, buffer));
			return false;
		}
	};
};

#endif /* PRODUCEREVENT_H_ */
