#ifndef STOPEVENT_H_
#define STOPEVENT_H_

#include "Event.h"

class StopEvent : public Event {

public:
	StopEvent(std::size_t t) : Event(t, "StopEvent", true) {};
	virtual ~StopEvent() = default;
};

#endif /* STOPEVENT_H_ */
