#ifndef EVENT_H_
#define EVENT_H_

#include <iostream>

class Event {
private:

	std::size_t time;
	std::string meta_name{"Event"};
	bool				stop_event{false};

public:

	Event(std::size_t t, std::string name = "Event", bool stop = false)
	: time{t}, meta_name{name}, stop_event{stop} {};

	virtual ~Event() = default;
	virtual std::string get_meta_name() { return meta_name; };
	virtual std::size_t get_time() { return time; };
	virtual bool is_stop_event() {return stop_event; }

	virtual bool execute() {
		std::cout << '(' << meta_name << ", " << time << ')' << std::endl;
		return true;
	};

	bool operator > (Event const &e) { return this->time > e.time; };
};

#endif /* EVENT_H_ */
