#include "EventQueue.h"
#include "StopEvent.h"
#include "CPSimulation.h"
#include <iostream>
#include <ctime>

int main() {

	EventQueue mySim;

	mySim.add(new Event(3));
	mySim.add(new Event(4));
	mySim.add(new Event(14));
	mySim.add(new Event(8));
	mySim.add(new Event(34));
	mySim.add(new Event(3));
	mySim.add(new StopEvent(13));

	mySim.run();

	std::srand(std::time(nullptr));
	CPSimulation cps(10000000, 1000000, 100);
	cps.run();


	return 0;
}
