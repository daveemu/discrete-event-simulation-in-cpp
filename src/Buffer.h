#ifndef BUFFER_H_
#define BUFFER_H_

#include <vector>

template <typename T>
class Buffer {
private:
	size_t max_size;
	size_t current_size{0};
	std::vector<T> buffer_data;

public:
	Buffer(size_t s) : max_size{s} {};

	virtual ~Buffer() = default;

	virtual void add(T item) {
		if(current_size < max_size) {
			buffer_data.push_back(item);
			++current_size;
		}
	}

	virtual void take() {
		if(current_size > 0) {
			buffer_data.pop_back();
			--current_size;
		}
	}

	virtual bool is_full() {
		return current_size == max_size;
	}

	virtual bool is_empty() {
		return !current_size;
	}

	virtual size_t size() {
		return current_size;
	}

	virtual size_t maxsize() {
		return max_size;
	}

	virtual void print() {
		for(size_t i = 0; i < current_size; i++) {
			std::cout << '#' << std::flush;
		}
		std::cout << std::endl;
	}

};

#endif /* BUFFER_H_ */
