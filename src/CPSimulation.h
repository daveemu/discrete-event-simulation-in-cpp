#ifndef CPSIMULATION_H_
#define CPSIMULATION_H_

#include "EventQueue.h"
#include "Buffer.h"
#include "ConsumerEvent.h"
#include "ProducerEvent.h"

class CPSimulation : public EventQueue {
private:
	std::size_t max_time;
	std::size_t max_items;
	std::size_t current_items{0};
	std::size_t current_time{0};
	Buffer<int> buffer;

public:
	CPSimulation(std::size_t time, std::size_t items, std::size_t buffer);
	virtual ~CPSimulation() = default;
	virtual void run() override;

};

#endif /* CPSIMULATION_H_ */
